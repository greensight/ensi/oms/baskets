<?php

return [
    'catalog' => [
        'catalog-cache' => [
            'base_uri' => env('CATALOG_CATALOG_CACHE_SERVICE_HOST') . "/api/v1",
        ],
        'offers' => [
            'base_uri' => env('CATALOG_OFFERS_SERVICE_HOST') . "/api/v1",
        ],
        'pim' => [
            'base_uri' => env('CATALOG_PIM_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'orders' => [
        'oms' => [
            'base_uri' => env('ORDERS_OMS_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'marketing' => [
        'marketing' => [
            'base_uri' => env('MARKETING_MARKETING_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
