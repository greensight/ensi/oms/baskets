<?php

namespace Tests;

use Ensi\CatalogCacheClient\Api\ElasticOffersApi as CatalogCacheOffersApi;
use Ensi\MarketingClient\Api\CalculatorsApi;
use Ensi\MarketingClient\Api\PromoCodesApi;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\PimClient\Api\ProductsApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // =============== Catalog ===============

    // region service Catalog Cache
    protected function mockCatalogCacheOffersApi(): MockInterface|CatalogCacheOffersApi
    {
        return $this->mock(CatalogCacheOffersApi::class);
    }
    // endregion

    // region service Offers
    protected function mockOffersOffersApi(): MockInterface|OffersApi
    {
        return $this->mock(OffersApi::class);
    }
    // endregion

    // region service PIM
    protected function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }
    // endregion

    // =============== Marketing ===============

    // region service Marketing
    protected function mockMarketingCalculatorsApi(): MockInterface|CalculatorsApi
    {
        return $this->mock(CalculatorsApi::class);
    }

    protected function mockMarketingPromoCodesApi(): MockInterface|PromoCodesApi
    {
        return $this->mock(PromoCodesApi::class);
    }
    // endregion

    // =============== Orders ===============

    // region service OMS
    protected function mockOmsOrdersApi(): MockInterface|OrdersApi
    {
        return $this->mock(OrdersApi::class);
    }
    // endregion
}
