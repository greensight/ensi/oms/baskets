<?php

use App\Domain\Common\Models\Setting;
use App\Http\ApiV1\Modules\Common\Tests\Factories\SettingsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/common/settings 200', function () {
    /** @var Setting $setting */
    $setting = Setting::query()->first();

    getJson("/api/v1/common/settings?filter[code]={$setting->code->value}")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $setting->id);
});

test('PATCH /api/v1/common/settings 200', function () {
    /** @var Setting $setting */
    $setting = Setting::factory()->create();

    $settingsQuery = SettingsRequestFactory::new()->make(["settings" => [['id' => $setting->id, 'value' => '123']]]);

    patchJson('/api/v1/common/settings', $settingsQuery)
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $setting->id)
        ->assertJsonPath('data.0.value', $settingsQuery['settings'][0]['value']);

    assertDatabaseHas((new Setting())->getTable(), [
        'id' => $setting->id,
        'value' => $settingsQuery['settings'][0]['value'],
    ]);
});

test('PATCH /api/v1/common/settings 400', function () {
    $value = "20";
    /** @var Setting $setting */
    $setting = Setting::factory()->create(['value' => $value]);

    $settingsQuery = SettingsRequestFactory::new()->make(["settings" => [['value' => "10"]]]);


    patchJson('/api/v1/common/settings', $settingsQuery)
        ->assertStatus(400);

    assertDatabaseHas((new Setting())->getTable(), [
        'id' => $setting->id,
        'value' => $value,
    ]);
});

test("GET /api/v1/common/settings filter success", function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
) {
    /** @var Setting $setting */
    $setting = Setting::factory()->create($value ? [$fieldKey => $value] : []);
    Setting::factory()->create();

    $filterKey = $filterKey ?: $fieldKey;
    $filterValue = $filterValue ?: $setting->{$fieldKey};
    getJson("/api/v1/common/settings?filter[$filterKey]=$filterValue")
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $setting->id);
})->with([
    ['id', null, null, null],
    ['name', 'полное имя', 'name_like', 'имя'],
    ['value', 10, null, null],
]);

test("GET /api/v1/common/settings filter range date success", function (string $fieldKey) {
    /** @var Setting $setting */
    $setting = Setting::factory()->create([$fieldKey => '2022-04-20']);
    Setting::factory()->create();

    $filterRangeDate = "filter[{$fieldKey}_gte]=2022-04-19&filter[{$fieldKey}_lte]=2022-04-21";
    getJson("/api/v1/common/settings?$filterRangeDate")
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $setting->id);
})->with([
    ['created_at'],
    ['updated_at'],
]);

test("GET /api/v1/common/settings sort success", function (string $sort) {
    Setting::factory()->create();
    getJson("/api/v1/common/settings?sort=$sort")->assertOk();
})->with([
    'id', 'code', 'name', 'value', 'created_at', 'updated_at',
]);
