<?php

namespace App\Http\ApiV1\Modules\Common\Resources;

use App\Domain\Common\Models\Setting;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Setting */
class SettingsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'value' => $this->value,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
