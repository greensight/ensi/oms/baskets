<?php

use App\Domain\Baskets\Models\Basket;
use App\Domain\Baskets\Models\BasketItem;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');


test('POST /api/v1/baskets/baskets:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = Basket::factory()
        ->count(5)
        ->create();
    $filteredModels = $models->sortBy('id');

    $basketItems = BasketItem::factory()->for($filteredModels->last())->count(3)->create();

    postJson("/api/v1/baskets/baskets:search", [
        "sort" => ["-id"],
        "include" => ["items"],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $filteredModels->last()->id)
        ->assertJsonCount($basketItems->count(), 'data.0.items');
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/baskets/baskets:search filter success', function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
    ?bool $always,
) {
    FakerProvider::$optionalAlways = $always;

    /** @var Basket $model */
    $model = Basket::factory()->create($value ? [$fieldKey => $value] : []);

    postJson("/api/v1/baskets/baskets:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id', null, null, null],
    ['customer_id', null, null, null, null],
    ['promo_code', 'unique-code-123', null, 'unique-code-123', true],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
], FakerProvider::$optionalDataset);

test('POST /api/v1/baskets/baskets:search sort success', function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Basket::factory()->create();

    postJson("/api/v1/baskets/baskets:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'customer_id',
    'promo_code',
    'updated_at',
    'created_at',
], FakerProvider::$optionalDataset);

test('POST /api/v1/baskets/baskets:search include success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Basket $model */
    $model = Basket::factory()->create();
    $basketItems = BasketItem::factory()->for($model)->count(3)->create();

    postJson("/api/v1/baskets/baskets:search", [
        "include" => ['items'],
    ])
        ->assertStatus(200)
        ->assertJsonCount($basketItems->count(), 'data.0.items');
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/baskets/baskets:search 400', function () {
    Basket::factory()
        ->count(5)
        ->create();

    postJson('/api/v1/baskets/baskets:search', [
        "filter" => ["test" => true],
    ])->assertStatus(400);
});
