<?php

use App\Domain\Baskets\Models\Basket;
use App\Domain\Baskets\Models\BasketItem;
use App\Domain\Baskets\Tests\BasketCalculateTestCase;
use App\Domain\Common\Tests\Factories\Marketing\CheckPromoCodeFactory;
use App\Http\ApiV1\Modules\Baskets\Tests\Factories\SetBasketCalculatePromoCodeRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\MarketingClient\Dto\PromoCodeApplyStatusEnum;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses(BasketCalculateTestCase::class);
uses()->group('component', 'baskets-promo-code');

test('POST /api/v1/baskets/baskets/calculate:promo-code 200', function (
    bool  $withBasket,
    ?bool $withPromoCode,
    ?bool $always,
) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase|BasketCalculateTestCase $this */
    $customerId = 1;
    $promoCode = 'code';

    $offerId = 1;
    $productId = 3;

    if ($withBasket) {
        /** @var Basket $basket */
        $basket = Basket::factory()->create(['customer_id' => $customerId]);
        BasketItem::factory()->for($basket)->create([
            'offer_id' => $offerId,
            'product_id' => $productId,
        ]);


        $this->mockCalculate(
            catalogCacheOffers: [
                ['id' => $offerId, 'product_id' => $productId],
            ],
            offers: [
                ['id' => $offerId],
            ],
            calculatedOffers: [
                ['offer_id' => $offerId],
            ],
            promoCode: $withPromoCode ? $promoCode : null,
            applyStatus: PromoCodeApplyStatusEnum::SUCCESS,
        );

        if (!is_null($withPromoCode)) {
            $this->mockMarketingPromoCodesApi()->allows([
                'checkPromoCode' => CheckPromoCodeFactory::new()->makeResponse([
                    'is_available' => $withPromoCode,
                    'promo_code_apply_status' => PromoCodeApplyStatusEnum::NOT_ACTIVE,
                ]),
            ]);
        }
    }

    $request = SetBasketCalculatePromoCodeRequestFactory::new()->make([
        'customer_id' => $customerId,
        'promo_code' => !is_null($withPromoCode) ? $promoCode : null,
    ]);


    if ($withBasket) {
        postJson('/api/v1/baskets/baskets/calculate:promo-code', $request)
            ->assertStatus(200)
            ->assertJsonPath(
                'data.promo_code_apply_status',
                $withPromoCode !== false ? PromoCodeApplyStatusEnum::SUCCESS : PromoCodeApplyStatusEnum::NOT_ACTIVE
            );

        assertDatabaseHas(Basket::class, [
            'customer_id' => $customerId,
            'promo_code' => $withPromoCode ? $promoCode : null,
        ]);
    } else {
        postJson('/api/v1/baskets/baskets/calculate:promo-code', $request)
            ->assertStatus(200)
            ->assertJsonPath('data.promo_code_apply_status', PromoCodeApplyStatusEnum::NOT_APPLIED);
    }

})->with([
    'withBasket unavailable promoCode' => [true, false],
    'withBasket available promoCode' => [true, true],
    'withBasket delete promoCode' => [true, null],
    'withOutBasket' => [false, null],
], FakerProvider::$optionalDataset);

test('POST /api/v1/baskets/baskets/calculate:promo-code 200 check apply status', function (
    string $checkStatus,
    string $calculateStatus,
    string $returnStatus,
    ?bool $always
) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase|BasketCalculateTestCase $this */
    $offerId = 1;
    $productId = 3;
    $qty = 2;
    $stockQty = $qty + 1;
    /** @var Basket $basket */
    $basket = Basket::factory()->create();
    BasketItem::factory()->for($basket)->create([
        'offer_id' => $offerId,
        'product_id' => $productId,
        'qty' => $qty,
    ]);

    $this->mockCalculate(
        catalogCacheOffers: [
            ['id' => $offerId, 'product_id' => $productId],
        ],
        offers: [
            ['id' => $offerId, 'qty' => $stockQty],
        ],
        calculatedOffers: [
            ['offer_id' => $offerId],
        ],
        applyStatus: $calculateStatus,
    );

    $this->mockMarketingPromoCodesApi()->allows([
        'checkPromoCode' => CheckPromoCodeFactory::new()->makeResponse([
            'is_available' => false,
            'promo_code_apply_status' => $checkStatus,
        ]),
    ]);

    $request = SetBasketCalculatePromoCodeRequestFactory::new()->make([
        'customer_id' => $basket->customer_id,
        'promo_code' => 'promo_code',
    ]);

    postJson('/api/v1/baskets/baskets/calculate:promo-code', $request)
        ->assertStatus(200)
        ->assertJsonCount($checkStatus !== PromoCodeApplyStatusEnum::SUCCESS ? 0 : 1, 'data.items')
        ->assertJsonPath('data.promo_code_apply_status', $returnStatus);
})->with([
    'saving the original error' => [PromoCodeApplyStatusEnum::NOT_FOUND, PromoCodeApplyStatusEnum::NOT_APPLIED, PromoCodeApplyStatusEnum::NOT_FOUND],
    'return error' => [PromoCodeApplyStatusEnum::SUCCESS, PromoCodeApplyStatusEnum::NOT_APPLIED, PromoCodeApplyStatusEnum::NOT_APPLIED],
], FakerProvider::$optionalDataset);
