<?php

namespace App\Http\ApiV1\Modules\Baskets\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class SetBasketCalculatePromoCodeRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'promo_code' => $this->faker->nullable()->word(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
