<?php

namespace App\Http\ApiV1\Modules\Baskets\Controllers;

use App\Http\ApiV1\Modules\Baskets\Queries\BasketItemsQuery;
use App\Http\ApiV1\Modules\Baskets\Resources\Baskets\BasketItemsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class BasketItemsController
{
    public function search(BasketItemsQuery $query, PageBuilderFactory $builderFactory): Responsable
    {
        return BasketItemsResource::collectPage(
            $builderFactory->fromQuery($query)->build()
        );
    }
}
