<?php

namespace App\Http\ApiV1\Modules\Baskets\Controllers;

use App\Domain\Baskets\Actions\Basket\DeleteBasketAction;
use App\Domain\Baskets\Actions\SetItems\SetItemsAction;
use App\Http\ApiV1\Modules\Baskets\Queries\BasketsQuery;
use App\Http\ApiV1\Modules\Baskets\Requests\DeleteBasketCustomerRequest;
use App\Http\ApiV1\Modules\Baskets\Requests\SearchBasketCustomerRequest;
use App\Http\ApiV1\Modules\Baskets\Requests\SetBasketCustomerItemRequest;
use App\Http\ApiV1\Modules\Baskets\Resources\Baskets\BasketsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class BasketsCustomerController
{
    public function setItem(SetBasketCustomerItemRequest $request, SetItemsAction $action): Responsable
    {
        $action->execute($request->convertToObject());

        return new EmptyResource();
    }

    public function searchOne(SearchBasketCustomerRequest $request, BasketsQuery $query): Responsable
    {
        return new BasketsResource($query->customer($request)->firstOrFail());
    }

    public function delete(DeleteBasketCustomerRequest $request, DeleteBasketAction $action, BasketsQuery $query): Responsable
    {
        $action->execute($query->customer($request)->firstOrFail());

        return new EmptyResource();
    }
}
