<?php

namespace App\Http\ApiV1\Modules\Baskets\Controllers;

use App\Domain\Baskets\Actions\Basket\SetBasketPromoCodeAction;
use App\Domain\Baskets\Actions\Calculators\CalculateBasketAction;
use App\Domain\Baskets\Actions\Calculators\Data\BasketCalculateData;
use App\Http\ApiV1\Modules\Baskets\Requests\SearchBasketCustomerRequest;
use App\Http\ApiV1\Modules\Baskets\Requests\SetBasketCalculatePromoCodeRequest;
use App\Http\ApiV1\Modules\Baskets\Resources\Calculators\CalculateBasketsResource;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BasketCalculatorsController
{
    public function searchOne(SearchBasketCustomerRequest $request, CalculateBasketAction $calculateAction): CalculateBasketsResource
    {
        $result = $calculateAction->execute(
            new BasketCalculateData(customerId: $request->getCustomerId(), include: $request->getInclude())
        );

        if ($result->basket === null) {
            throw new NotFoundHttpException('Не найдена корзина');
        }

        return new CalculateBasketsResource($result);
    }

    public function promoCode(SetBasketCalculatePromoCodeRequest $request, SetBasketPromoCodeAction $action, CalculateBasketAction $calculateAction): CalculateBasketsResource
    {
        $result = $action->execute($request->getCustomerId(), $request->getPromoCode());

        $data = new BasketCalculateData(
            customerId: $request->getCustomerId(),
            basket: $result->basket,
            promoCodeApplyStatus: $result->applyStatus
        );

        return new CalculateBasketsResource($calculateAction->execute($data));
    }
}
