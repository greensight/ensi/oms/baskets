<?php

namespace App\Http\ApiV1\Modules\Baskets\Controllers;

use App\Http\ApiV1\Modules\Baskets\Queries\BasketsQuery;
use App\Http\ApiV1\Modules\Baskets\Resources\Baskets\BasketsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class BasketsController
{
    public function search(BasketsQuery $query, PageBuilderFactory $builderFactory): Responsable
    {
        return BasketsResource::collectPage(
            $builderFactory->fromQuery($query)->build()
        );
    }
}
