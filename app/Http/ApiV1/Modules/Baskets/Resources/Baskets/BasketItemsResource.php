<?php

namespace App\Http\ApiV1\Modules\Baskets\Resources\Baskets;

use App\Domain\Baskets\Models\BasketItem;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin BasketItem */
class BasketItemsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,

            'basket_id' => $this->basket_id,
            'offer_id' => $this->offer_id,
            'product_id' => $this->product_id,
            'qty' => $this->qty,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

        ];
    }
}
