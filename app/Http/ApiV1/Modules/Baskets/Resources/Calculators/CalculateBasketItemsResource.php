<?php

namespace App\Http\ApiV1\Modules\Baskets\Resources\Calculators;

use App\Domain\Baskets\Actions\Calculators\Data\BasketItemInfoData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin BasketItemInfoData */
class CalculateBasketItemsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'offer_id' => $this->catalogCacheOffer->getId(),
            'product_id' => $this->catalogCacheOffer->getProductId(),
            'name' => $this->catalogCacheOffer->getName(),
            'code' => $this->catalogCacheOffer->getCode(),
            'barcode' => $this->catalogCacheOffer->getBarcode(),
            'vendor_code' => $this->catalogCacheOffer->getVendorCode(),
            'type' => $this->catalogCacheOffer->getType(),
            'is_adult' => $this->catalogCacheOffer->getIsAdult(),
            'uom' => $this->catalogCacheOffer->getUom(),
            'tariffing_volume' => $this->catalogCacheOffer->getTariffingVolume(),
            'order_step' => $this->catalogCacheOffer->getOrderStep(),
            'order_minvol' => $this->catalogCacheOffer->getOrderMinvol(),
            'main_image' => $this->catalogCacheOffer->getMainImageFile()?->getUrl(),
            'qty' => $this->data->qty,
            'stock_qty' => $this->stock?->getQty() ?? 0,
            'price' => $this->getPrice(),
            'price_per_one' => $this->getPricePerOne(),
            'cost' => $this->getCost(),
            'cost_per_one' => $this->getCostPerOne(),
            'discount' => $this->getDiscount(),
            'discount_per_one' => $this->getDiscountPerOne(),
            'discounts' => DiscountsResource::collection($this->whenNotNull($this->getDiscounts())),
            'images' => ProductImagesResource::collection($this->whenNotNull($this->catalogCacheOffer->getImages())),
            'brand' => ProductBrandsResource::make($this->whenNotNull($this->catalogCacheOffer->getBrand())),
            'nameplates' => ProductNameplatesResource::collection($this->whenNotNull($this->catalogCacheOffer->getNameplates())),
        ];
    }
}
