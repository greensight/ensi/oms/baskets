<?php

namespace App\Http\ApiV1\Modules\Baskets\Resources\Calculators;

use App\Domain\Baskets\Actions\Calculators\Data\DiscountData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin DiscountData */
class DiscountsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'value' => $this->value,
            'value_type' => $this->valueType,
        ];
    }
}
