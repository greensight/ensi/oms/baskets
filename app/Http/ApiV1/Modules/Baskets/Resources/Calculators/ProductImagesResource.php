<?php

namespace App\Http\ApiV1\Modules\Baskets\Resources\Calculators;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\ElasticImage;

/**
 * @mixin ElasticImage
 */
class ProductImagesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'sort' => $this->getSort(),
            'name' => $this->getName(),
            'url' => $this->getUrl(),
        ];
    }
}
