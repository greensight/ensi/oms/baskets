<?php

namespace App\Http\ApiV1\Modules\Baskets\Queries;

use App\Domain\Baskets\Models\BasketItem;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\NumericFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * @template T
 * @mixin T
 */
class BasketItemsQuery extends QueryBuilder
{
    public function __construct()
    {
        /** @var T */
        $query = BasketItem::query();

        parent::__construct($query);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('basket_id'),
            AllowedFilter::exact('offer_id'),
            AllowedFilter::exact('product_id'),
            ...NumericFilter::make('qty')->lte()->gte(),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->allowedSorts(['id', 'basket_id', 'offer_id', 'product_id', 'qty', 'created_at', 'updated_at']);

        $this->defaultSort('id');
    }
}
