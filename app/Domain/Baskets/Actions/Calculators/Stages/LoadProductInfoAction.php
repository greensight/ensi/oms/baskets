<?php

namespace App\Domain\Baskets\Actions\Calculators\Stages;

use App\Domain\Baskets\Actions\Calculators\Data\BasketItemInfoData;
use App\Domain\Baskets\Actions\Calculators\Data\CalculatorBasketContext;
use App\Domain\Baskets\Actions\Calculators\Data\DiscountData;
use App\Domain\Baskets\Models\Basket;
use App\Exceptions\ValidateException;
use Ensi\CatalogCacheClient\Api\ElasticOffersApi as CCOffersApi;
use Ensi\CatalogCacheClient\Dto\RequestBodyPagination as CCRequestPagination;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersRequest as CCSearchOffersRequest;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersResponse as CCSearchOffersResponse;
use Ensi\MarketingClient\Api\CalculatorsApi;
use Ensi\MarketingClient\Dto\CalculateCheckoutOffer;
use Ensi\MarketingClient\Dto\CalculateCheckoutRequest;
use Ensi\MarketingClient\Dto\CalculateCheckoutResponse;
use Ensi\MarketingClient\Dto\CalculatedOffer;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\RequestBodyPagination as OffersRequestPagination;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\OffersClient\Dto\SearchOffersResponse;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\Utils;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class LoadProductInfoAction
{
    protected Collection $catalogCacheOffers;
    protected Collection $offers;
    protected Collection $prices;
    protected ?string $usedPromoCode = null;
    private ?string $promoCodeApplyStatus = null;

    public function __construct(
        protected CCOffersApi    $catalogCacheOffersApi,
        protected OffersApi      $offersApi,
        protected CalculatorsApi $calculatorsApi,
    ) {
        $this->catalogCacheOffers = collect();
        $this->offers = collect();
        $this->prices = collect();
    }

    public function execute(CalculatorBasketContext $context): void
    {
        $items = $context->basket->items;

        $offerIds = Arr::pluck($items, 'offer_id');

        if (!$offerIds) {
            return;
        }

        Utils::all([
            $this->loadCatalogCacheOffers($offerIds, $context),
            $this->loadOffers($offerIds),
        ])->wait();

        $this->loadMarketing($context->basket)->wait();

        foreach ($items as $item) {
            $catalogCacheOffer = $this->catalogCacheOffers->get($item->offer_id);
            if (!$catalogCacheOffer) {
                throw new ValidateException("Для товара {$item->offer_id} не найден оффер в Elastic");
            }

            /** @var Offer $offer */
            $offer = $this->offers->get($item->offer_id);

            $stock = collect($offer->getStocks())->first();

            # todo: check stock
            //            if (!$stock) {
            //                throw new ValidateException("Для товара {$item->offer_id} не найден сток");
            //            }

            $basketInfoData = new BasketItemInfoData(
                data: $item,
                catalogCacheOffer: $catalogCacheOffer,
                stock: $stock,
            );

            /** @var CalculatedOffer $price */
            $price = $this->prices->get($item->offer_id);
            $basketInfoData->setCostPerOne($price->getCost());
            $basketInfoData->setPricePerOne($price->getPrice());

            foreach ($price->getDiscounts() ?? [] as $discount) {
                $discountData = new DiscountData(
                    valueType: $discount->getValueType(),
                    value: $discount->getValue(),
                );
                $basketInfoData->setDiscount($discountData);
            }
            $context->setBasketItemInfo($basketInfoData);
        }

        $context->usedPromoCode = $this->usedPromoCode;

        if ($this->promoCodeApplyStatus) {
            $context->promoCodeApplyStatus = $this->promoCodeApplyStatus;
        }
    }

    protected function loadCatalogCacheOffers(array $offerIds, CalculatorBasketContext $context): PromiseInterface
    {
        $request = new CCSearchOffersRequest();
        $request->setFilter((object)['offer_id' => $offerIds]);

        $request->setInclude($context->data->include);
        $request->setPagination((new CCRequestPagination())->setLimit(count($offerIds)));

        return $this->catalogCacheOffersApi->searchElasticOffersAsync($request)->then(function (CCSearchOffersResponse $response) {
            $this->catalogCacheOffers = collect($response->getData())->keyBy('id');

            return $response;
        });
    }

    protected function loadOffers(array $offerIds): PromiseInterface
    {
        $request = new SearchOffersRequest();
        $request->setFilter((object)['id' => $offerIds]);
        $request->setInclude(['stocks']);
        $request->setPagination((new OffersRequestPagination())->setLimit(count($offerIds)));

        return $this->offersApi->searchOffersAsync($request)->then(function (SearchOffersResponse $response) {
            $this->offers = collect($response->getData())->keyBy('id');

            return $response;
        });
    }

    protected function loadMarketing(Basket $basket): PromiseInterface
    {
        $request = new CalculateCheckoutRequest();
        $request->setPromoCode($basket->promo_code);

        $offers = [];
        foreach ($basket->items as $item) {
            /** @var Offer|null $offerDB */
            $offerDB = $this->offers->get($item->offer_id);

            if (!$offerDB) {
                throw new ValidateException("Для товара {$item->offer_id} не найден оффер");
            }

            $offer = new CalculateCheckoutOffer();
            $offer->setOfferId($item->offer_id);
            $offer->setProductId($item->product_id);
            $offer->setCost($offerDB->getPrice());

            $offers[] = $offer;
        }
        $request->setOffers($offers);

        return $this->calculatorsApi->calculateCheckoutAsync($request)->then(function (CalculateCheckoutResponse $response) {
            $data = $response->getData();
            $this->usedPromoCode = $data->getPromoCode();
            $this->promoCodeApplyStatus = $data->getPromoCodeApplyStatus();
            $this->prices = collect($data->getOffers())->keyBy('offer_id');
        });
    }
}
