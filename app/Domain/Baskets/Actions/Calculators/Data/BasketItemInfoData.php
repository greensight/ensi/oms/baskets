<?php

namespace App\Domain\Baskets\Actions\Calculators\Data;

use App\Domain\Baskets\Models\BasketItem;
use Ensi\CatalogCacheClient\Dto\ElasticOffer as CatalogCacheOffer;
use Ensi\OffersClient\Dto\Stock;

class BasketItemInfoData
{
    protected int $pricePerOne = 0;
    protected int $costPerOne = 0;
    protected array $discounts = [];

    public function __construct(
        public BasketItem $data,
        public CatalogCacheOffer $catalogCacheOffer,
        public ?Stock $stock,
    ) {
    }

    public function setCostPerOne(int $costPerOne): void
    {
        $this->costPerOne = $costPerOne;
    }

    public function setPricePerOne(int $pricePerOne): void
    {
        $this->pricePerOne = $pricePerOne;
    }

    public function getCostPerOne(): int
    {
        return $this->costPerOne;
    }

    public function getCost(): int
    {
        return (int) round($this->getCostPerOne() * $this->data->qty);
    }

    public function getPricePerOne(): int
    {
        return $this->pricePerOne;
    }

    public function getPrice(): int
    {
        return (int) round($this->getPricePerOne() * $this->data->qty);
    }

    public function getDiscountPerOne(): int
    {
        return $this->getCostPerOne() - $this->getPricePerOne();
    }

    public function getDiscount(): int
    {
        return $this->getCost() - $this->getPrice();
    }

    public function setDiscount(DiscountData $discountData): void
    {
        $this->discounts[] = $discountData;
    }

    public function getDiscounts(): array
    {
        return $this->discounts;
    }
}
