<?php

namespace App\Domain\Baskets\Actions\Calculators\Data;

use App\Domain\Baskets\Models\Basket;

class BasketCalculateData
{
    public function __construct(
        public ?int $customerId = null,
        public ?Basket $basket = null,
        public ?string $promoCodeApplyStatus = null,
        public array $include = [],
    ) {
    }
}
