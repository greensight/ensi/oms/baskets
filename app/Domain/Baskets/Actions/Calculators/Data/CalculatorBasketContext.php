<?php

namespace App\Domain\Baskets\Actions\Calculators\Data;

use App\Domain\Baskets\Models\Basket;
use Ensi\MarketingClient\Dto\PromoCodeApplyStatusEnum;
use Illuminate\Support\Collection;

class CalculatorBasketContext
{
    public ?Basket $basket;
    /** @var Collection<BasketItemInfoData> */
    protected Collection $items;
    public ?string $usedPromoCode = null;
    public ?string $promoCodeApplyStatus = null;

    public function __construct(public BasketCalculateData $data)
    {
        $this->items = collect();
        $this->loadBasket();
        $this->promoCodeApplyStatus = $this->data->promoCodeApplyStatus;
    }

    /**
     * @return bool - доступен промокод при установке, если нет, но нет смысла пересчитывать корзину
     */
    public function isNotAvailablePromoCode(): bool
    {
        return $this->promoCodeApplyStatus !== PromoCodeApplyStatusEnum::SUCCESS && $this->promoCodeApplyStatus !== null;
    }

    protected function loadBasket(): void
    {
        $this->basket = $this->data->basket ?? Basket::query()
            ->where('customer_id', $this->data->customerId)
            ->first();

        $this->basket?->loadMissing('items');
    }

    public function setBasketItemInfo(BasketItemInfoData $offerInfoData): void
    {
        $this->items->put($offerInfoData->catalogCacheOffer->getId(), $offerInfoData);
    }

    /**
     * @return Collection<BasketItemInfoData>
     */
    public function items(): Collection
    {
        return $this->items
            ->sort(function (BasketItemInfoData $current, BasketItemInfoData $next) {
                if ($current->data->created_at->eq($next->data->created_at)) {
                    return $current->data->offer_id <=> $next->data->offer_id;
                }

                return $current->data->created_at <=> $next->data->created_at;
            });
    }
}
