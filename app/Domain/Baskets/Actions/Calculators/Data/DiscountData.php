<?php

namespace App\Domain\Baskets\Actions\Calculators\Data;

class DiscountData
{
    /**
     * @param $valueType - тип скидки
     * @param $value - значение скидки
     */
    public function __construct(
        public int $valueType,
        public int $value,
    ) {
    }
}
