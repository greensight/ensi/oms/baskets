<?php

namespace App\Domain\Baskets\Actions\Calculators;

use App\Domain\Baskets\Actions\Calculators\Data\BasketCalculateData;
use App\Domain\Baskets\Actions\Calculators\Data\CalculatorBasketContext;
use App\Domain\Baskets\Actions\Calculators\Stages\LoadProductInfoAction;
use App\Domain\Baskets\Actions\Calculators\Stages\PromoCodeUpdateAction;
use Ensi\MarketingClient\Dto\PromoCodeApplyStatusEnum;

class CalculateBasketAction
{
    protected CalculatorBasketContext $context;

    public function __construct(
        protected LoadProductInfoAction $loadProductInfoAction,
        protected PromoCodeUpdateAction $promoCodeUpdateAction,
    ) {
    }

    public function execute(BasketCalculateData $data): CalculatorBasketContext
    {
        $context = new CalculatorBasketContext($data);

        if ($context->isNotAvailablePromoCode()) {
            return $context;
        }

        if ($context->basket == null) {
            $context->promoCodeApplyStatus = PromoCodeApplyStatusEnum::NOT_APPLIED;

            return $context;
        }

        $this->loadProductInfoAction->execute($context);
        $this->promoCodeUpdateAction->execute($context);

        return $context;
    }
}
