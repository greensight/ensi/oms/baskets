<?php

namespace App\Domain\Baskets\Actions\SetItems\Data;

use Ensi\OffersClient\Dto\Offer;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductTypeEnum;

class ProductInfoData
{
    public Offer $offer;
    public Product $product;

    public function getMinQty(): float
    {
        return $this->product->getType() === ProductTypeEnum::WEIGHT ? $this->product->getOrderMinvol() ?? 0 : 0;
    }
}
