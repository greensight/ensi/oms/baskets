<?php

namespace App\Domain\Baskets\Actions\SetItems\Stages;

use App\Domain\Baskets\Actions\SetItems\Data\ProductInfoData;
use App\Domain\Baskets\Actions\SetItems\Data\SetItemsContext;
use App\Exceptions\ValidateException;
use Arr;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Illuminate\Support\Collection;

class LoadProductInfoAction
{
    public function __construct(
        protected OffersApi $offersApi,
        protected ProductsApi $productsApi,
    ) {
    }

    public function execute(SetItemsContext $context): void
    {
        $offerIds = Arr::pluck($context->data->addItems(), 'offerId');
        $offers = $this->loadOffers($offerIds);

        $productIds = $offers->pluck('product_id')->unique()->values()->toArray();
        $products = $this->loadProducts($productIds);

        if ($products->count() !== count($productIds)) {
            throw new ValidateException('В запросе присутствует неактивный товар');
        }

        foreach ($context->data->addItems() as $item) {
            /** @var Offer|null $offer */
            $offer = $offers->get($item->offerId);
            if (!$offer) {
                throw new ValidateException("Для товара {$item->offerId} не найден оффер");
            }

            if (!$offer->getIsRealActive()) {
                throw new ValidateException("Оффер {$item->offerId} не может быть добавлен в корзину.");
            }

            /** @var Product|null $product */
            $product = $products->get($offer->getProductId());
            if (!$product) {
                throw new ValidateException("Для товара {$item->offerId} не найден продукт");
            }

            # todo: check stock
            //            /** @var Stock|null $stock */
            //            $stock = collect($offer->getStocks())->first();
            //            if (!$stock) {
            //                throw new ValidateException("Для товара {$item->offerId} не найден сток");
            //            }
            //            if ($stock->getQty() < $item->qty) {
            //                throw new ValidateException("Кол-ва товара {$item->offerId} недостаточно на складе");
            //            }

            $productInfo = new ProductInfoData();
            $productInfo->offer = $offer;
            $productInfo->product = $product;

            $context->setProductInfo($item->offerId, $productInfo);
        }
    }

    /** @return Collection<Product> */
    protected function loadProducts(array $productIds): Collection
    {
        $request = new SearchProductsRequest();
        $request->setFilter((object)[
            'allow_publish' => true,
            'id' => $productIds,
        ]);

        return collect($this->productsApi->searchProducts($request)->getData())->keyBy('id');
    }

    /** @return Collection<Offer> */
    protected function loadOffers(array $offerIds): Collection
    {
        $searchOffersRequest = new SearchOffersRequest();
        $searchOffersRequest->setFilter((object)[
            'id' => $offerIds,
        ]);
        $searchOffersRequest->setInclude(['stocks']);

        return collect($this->offersApi->searchOffers($searchOffersRequest)->getData())->keyBy('id');
    }
}
