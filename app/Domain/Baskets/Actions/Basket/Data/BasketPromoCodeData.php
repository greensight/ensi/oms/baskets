<?php

namespace App\Domain\Baskets\Actions\Basket\Data;

use App\Domain\Baskets\Models\Basket;

class BasketPromoCodeData
{
    public function __construct(public string $applyStatus, public ?Basket $basket = null)
    {
    }
}
