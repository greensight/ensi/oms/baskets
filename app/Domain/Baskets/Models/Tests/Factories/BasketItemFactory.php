<?php

namespace App\Domain\Baskets\Models\Tests\Factories;

use App\Domain\Baskets\Models\BasketItem;
use Ensi\LaravelTestFactories\BaseModelFactory;

class BasketItemFactory extends BaseModelFactory
{
    protected $model = BasketItem::class;

    public function definition(): array
    {
        return [
            'basket_id' => $this->faker->modelId(),
            'offer_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'qty' => $this->faker->randomFloat(2, 0, 100),
        ];
    }
}
