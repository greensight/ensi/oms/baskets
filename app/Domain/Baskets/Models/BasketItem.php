<?php

namespace App\Domain\Baskets\Models;

use App\Domain\Baskets\Models\Tests\Factories\BasketItemFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $basket_id Basket ID
 * @property int $offer_id Offer ID
 * @property int $product_id Product ID
 * @property float $qty Product quantity
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read Basket $basket
 */
class BasketItem extends Model
{
    protected $table = 'basket_items';

    protected $casts = [
        'qty' => 'float',
    ];

    public static function factory(): BasketItemFactory
    {
        return BasketItemFactory::new();
    }

    public function basket(): BelongsTo
    {
        return $this->belongsTo(Basket::class);
    }
}
