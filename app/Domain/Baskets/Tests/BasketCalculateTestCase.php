<?php

namespace App\Domain\Baskets\Tests;

use App\Domain\Common\Tests\Factories\Catalog\CatalogCacheOfferFactory;
use App\Domain\Common\Tests\Factories\Catalog\OffersOfferFactory;
use App\Domain\Common\Tests\Factories\Catalog\StockFactory;
use App\Domain\Common\Tests\Factories\Marketing\CalculateCheckoutFactory;
use Ensi\LaravelTestFactories\PromiseFactory;
use Mockery\MockInterface;
use Tests\ComponentTestCase;

/**
 * @mixin ComponentTestCase
 */
trait BasketCalculateTestCase
{
    public function mockCalculate(
        array $catalogCacheOffers,
        array $offers,
        array $calculatedOffers,
        ?string $promoCode = null,
        ?string $applyStatus = null,
        MockInterface $mockOffersOffersApi = null,
    ): void {
        $this->mockCatalogCacheOffersApi()->allows([
            'searchElasticOffersAsync' => PromiseFactory::make(CatalogCacheOfferFactory::new()->makeResponseSearch($catalogCacheOffers)),
        ]);

        $offersResponse = [];
        foreach ($offers as $offer) {
            if (is_array($offer)) {
                $offerFactory = OffersOfferFactory::new();
                if (array_key_exists('qty', $offer)) {
                    $offerFactory->withStock(StockFactory::new()->make(['qty' => $offer['qty']]));
                    unset($offer['qty']);
                }
                $offersResponse[] = $offerFactory->make($offer);
            } else {
                $offersResponse[] = $offer;
            }
        }

        ($mockOffersOffersApi ?: $this->mockOffersOffersApi())
            ->shouldReceive('searchOffersAsync')
            ->andReturn(PromiseFactory::make(OffersOfferFactory::new()->makeResponseSearch($offersResponse)));

        $this->mockMarketingCalculatorsApi()->allows([
            'calculateCheckoutAsync' => PromiseFactory::make(CalculateCheckoutFactory::new()->withOffers($calculatedOffers)->makeResponse([
                'promo_code' => $promoCode,
                'promo_code_apply_status' => $applyStatus,
            ])),
        ]);
    }
}
