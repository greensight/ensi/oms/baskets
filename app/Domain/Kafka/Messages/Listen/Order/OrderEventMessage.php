<?php

namespace App\Domain\Kafka\Messages\Listen\Order;

use Illuminate\Support\Fluent;
use RdKafka\Message;

/**
 * Class DeliveryOrderEventMessage
 * @package App\Domain\Kafka\Messages\Listen
 *
 * @property array $dirty
 * @property OrderPayload $attributes
 * @property string $event
 */
class OrderEventMessage extends Fluent
{
    public const CREATE = 'create';
    public const UPDATE = 'update';
    public const DELETE = 'delete';

    public function __construct($attributes = [])
    {
        $attributes['attributes'] = new OrderPayload($attributes['attributes']);
        parent::__construct($attributes);
    }

    public static function makeFromRdKafka(Message $message): self
    {
        return new self(json_decode($message->payload, true));
    }
}
