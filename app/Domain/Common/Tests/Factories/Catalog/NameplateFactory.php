<?php

namespace App\Domain\Common\Tests\Factories\Catalog;

use Ensi\CatalogCacheClient\Dto\ElasticNameplate;
use Ensi\LaravelTestFactories\BaseApiFactory;

class NameplateFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->text(50),
            'code' => $this->faker->unique()->text(50),
            'background_color' => $this->faker->hexColor(),
            'text_color' => $this->faker->hexColor(),
        ];
    }

    public function make(array $extra = []): ElasticNameplate
    {
        return new ElasticNameplate($this->makeArray($extra));
    }
}
