<?php

namespace App\Domain\Common\Tests\Factories\Catalog;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LaravelTestFactories\FactoryMissingValue;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductTariffingVolumeEnum;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\ProductUomEnum;
use Ensi\PimClient\Dto\SearchProductsResponse;
use Illuminate\Support\Collection;

class ProductFactory extends BaseApiFactory
{
    public ?Collection $images = null;
    public ?Collection $attributes = null;

    protected function definition(): array
    {
        $type = $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues());
        $isWeigh = $type === ProductTypeEnum::WEIGHT;

        return [
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->slug,
            'description' => $this->faker->text,
            'type' => $type,
            'status_id' => $this->faker->modelId(),
            'status_comment' => $this->faker->optional()->sentence(),
            'allow_publish' => $this->faker->boolean,

            'external_id' => $this->faker->unique()->numerify('######'),
            'barcode' => $this->faker->ean13(),
            'vendor_code' => $this->faker->numerify('###-###-###'),

            'weight' => $this->faker->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->randomFloat(3, 1, 100),
            'length' => $this->faker->randomFloat(2, 1, 1000),
            'height' => $this->faker->randomFloat(2, 1, 1000),
            'width' => $this->faker->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean,
            'uom' => $this->faker->nullable($isWeigh)->randomElement(ProductUomEnum::getAllowableEnumValues()),
            'tariffing_volume' => $this->faker->nullable($isWeigh)->randomElement(ProductTariffingVolumeEnum::getAllowableEnumValues()),
            'order_step' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 1000),
            'order_minvol' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 100),
            'picking_weight_deviation' => $this->faker->nullable($isWeigh)->randomFloat(2, 0, 100),

            'category_id' => $this->faker->modelId(),
            'images' => $this->executeNested($this->images, new FactoryMissingValue()),
            'attributes' => $this->whenNotNull($this->attributes, $this->attributes?->all()),
        ];
    }

    public function make(array $extra = []): Product
    {
        return new Product($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchProductsResponse
    {
        return $this->generateResponseSearch(SearchProductsResponse::class, $extras, $count, $pagination);
    }

    public function makeResponseSearchOne(array $extra = []): SearchProductsResponse
    {
        return new SearchProductsResponse(['data' => [$this->make($extra)]]);
    }

    public function makeResponseEmpty(): SearchProductsResponse
    {
        return new SearchProductsResponse(['data' => []]);
    }
}
