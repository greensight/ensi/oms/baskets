<?php

namespace App\Domain\Common\Tests\Factories\Catalog;

use Ensi\CatalogCacheClient\Dto\ElasticOffer;
use Ensi\CatalogCacheClient\Dto\ElasticOfferResponse;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersResponse;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\File;
use Ensi\PimClient\Dto\ProductTariffingVolumeEnum;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\ProductUomEnum;

class CatalogCacheOfferFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $type = $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues());
        $isWeigh = $type === ProductTypeEnum::WEIGHT;

        return [
            'id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'active' => $this->faker->boolean,
            'main_image_file' => new File(EnsiFile::factory()->make()),
            'category_id' => $this->faker->modelId(),
            'brand_id' => $this->faker->nullable()->modelId(),
            'name' => $this->faker->sentence(),
            'code' => $this->faker->slug(),
            'description' => $this->faker->nullable()->text,
            'type' => $type,
            'vendor_code' => $this->faker->nullable()->numerify('###-###-###'),
            'barcode' => $this->faker->nullable()->ean13(),
            'weight' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'width' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'height' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'length' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean,
            'uom' => $this->faker->nullable($isWeigh)->randomElement(ProductUomEnum::getAllowableEnumValues()),
            'tariffing_volume' => $this->faker->nullable($isWeigh)->randomElement(ProductTariffingVolumeEnum::getAllowableEnumValues()),
            'order_step' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 1000),
            'order_minvol' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 100),
            'price' => $this->faker->randomNumber(),
            'cost' => $this->faker->randomNumber(),
            'nameplates' => $this->faker->randomList(fn () => NameplateFactory::new()->make(), 1, 3),
        ];
    }

    public function make(array $extra = []): ElasticOffer
    {
        return new ElasticOffer($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ElasticOfferResponse
    {
        return new ElasticOfferResponse(['data' => $this->make($extra),]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchElasticOffersResponse
    {
        return $this->generateResponseSearch(SearchElasticOffersResponse::class, $extras, $count, $pagination);
    }
}
