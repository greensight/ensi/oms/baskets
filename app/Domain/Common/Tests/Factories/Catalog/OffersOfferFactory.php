<?php

namespace App\Domain\Common\Tests\Factories\Catalog;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\OfferResponse;
use Ensi\OffersClient\Dto\SearchOffersResponse;
use Ensi\OffersClient\Dto\Stock;

class OffersOfferFactory extends BaseApiFactory
{
    protected array $stocks = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'price' => $this->faker->numberBetween(1, 100000),
            'is_real_active' => true,
        ];

        if ($this->stocks) {
            $definition['stocks'] = $this->stocks;
        }

        return $definition;
    }

    public function withStock(?Stock $stock): self
    {
        $this->stocks[] = $stock ?: StockFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Offer
    {
        return new Offer($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): OfferResponse
    {
        return new OfferResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchOffersResponse
    {
        return $this->generateResponseSearch(SearchOffersResponse::class, $extras, $count, $pagination);
    }
}
