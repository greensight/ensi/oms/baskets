<?php

namespace App\Domain\Common\Tests\Factories\Orders;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\OmsClient\Dto\OrderResponse;
use Ensi\OmsClient\Dto\SearchOrdersResponse;

class OrderFactory extends BaseApiFactory
{
    protected array $items = [];
    protected array $deliveries = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'number' => $this->faker->unique()->numerify('######'),
            'customer_id' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->items) {
            $definition['items'] = $this->items;
        }

        return $definition;
    }

    public function withItem(?OrderItem $item = null): self
    {
        $this->items[] = $item ?: OrderItemFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Order
    {
        return new Order($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): OrderResponse
    {
        return new OrderResponse(['data' => $this->make($extra),]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchOrdersResponse
    {
        return $this->generateResponseSearch(SearchOrdersResponse::class, $extras, $count, $pagination);
    }
}
